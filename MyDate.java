public class MyDate {
	
	int day,month,year;
	
	
	MyDate(int day,int month,int year)
	{
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public String toString() {
		String year =  String.valueOf(this.year);
		String month = String.valueOf(this.month);
		String day = String.valueOf(this.day);
		if(this.month <10 && this.day <10) 
		{
			return year + "-0" + month + "-0" + day;
			
		}
		else if(this.month <10 ) 
		{
			return year + "-0" + month + "-" + day;
		}
		else if(this.day <10 ) 
		{
			return year + "-" + month + "-0" + day;
		}
		
		else 
		{
			return year + "-" + month + "-" + day;
		}
	}
	
	
	public void incrementDay() //1
	{
		boolean secMonth = (this.year % 4 == 0 );
		boolean month31 = (this.month == 1 || this.month==3 || this.month==5 || this.month == 7 || this.month ==8 ||this.month==10 );
		boolean lastMonth = (this.month == 12);
		boolean month30 = (this.month == 4 || this.month == 6 || this.month == 8 || this.month == 9 );
		
		if(secMonth && this.month == 2 ) 
		{
			this.month = 3;
			this.day = 1;
			
		}
		else if(secMonth == false && this.month == 2) 
		{
			this.month = 3;
			this.day = 1;
		}
		
		else if (month31) 
		{
			this.month+=1;
			this.day = 1;
		}
		
		else if(lastMonth) 
		{
			this.year+=1;
			this.month=1;
			this.day = 1;
			
		}
		else if(month30) 
		{
			this.day = 1;
			this.month+=1;
		}
	}

    public void incrementDay(int value) //11
	{
		boolean secMonth = (this.year % 4 == 0 );
		boolean month31 = (this.month == 1 || this.month==3 || this.month==5 || this.month == 7 || this.month ==8 ||this.month==10 );
		boolean lastMonth = (this.month == 12);
		boolean month30 = (this.month == 4 || this.month == 6 || this.month == 8 || this.month == 9 );
		
		if(secMonth && this.month == 2 ) 
		{
			this.month = 3;
			this.day = 1;
			
		}
		else if(this.month == 2) 
        {

			this.month = 3;
			this.day = 1;
		}
		
		else if (month31 && this.day ==31) 
		{
			this.month+=1;
			this.day = 1;
		}
		
		else if(lastMonth && this.day ==31) 
		{
			this.year+=1;
			this.month=1;
			this.day = 1;
			
		}
		else if(month30 && this.day ==30) 
		{
			this.day = 1;
			this.month+=1;
		}
        else 
        {
            this.day +=1;

        }
        value-=1;
        if(value!=0)
        {
            incrementDay(value);
        }
	}
	
	public void incrementYear(int value) //2
	{
		if(value % 4 == 0) 
		{
			this.year += value;
			this.day +=1;
		}
		else 
		{
			this.year+=value;
			
		}
	}
	
	public void decrementDay() //-1
	{
		boolean secMonth = (this.year % 4 == 0 );
		boolean month31 = (this.month == 1 || this.month==3 || this.month==5 || this.month == 7 || this.month ==8 ||this.month==10 );
		boolean lastMonth = (this.month == 12);
		boolean month30 = (this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11 );
		
		if(this.day== 1 && this.month == 1) 
		{
			this.day =31;
			this.month= 12;
			
		}
		
		else if (this.day == 1 && this.month==3 && this.year % 4 == 0) 
		{
			this.day = 29;
			this.month-=1;
		}
		
		else if (this.day==1 && this.month == 3 && this.year % 4 != 0 ) 
		{
			this.day = 28;
			this.month = 2;
		}
		
		else if(month30 && this.day == 1) {
			this.day = 31;
			this.month -=1;
		}
		
		else {
			this.day-=1;
		}
	}
	
	public void decrementYear() { //-2
        if(this.month==2 && this.day==29) {
            this.year-=1;
            this.day=28;
        }
        else {
            this.year-=1;
        }
    }
	
    public void decrementYear(int value) { //-22
		if(this.year % 4 == 0 && this.day > 29 && this.month == 2) 
		{
			this.year -=value;
			this.day =29;
		}
        else if(this.year % 4 != 0 && this.day >28 && this.month == 2) 
		{
			this.year -=value;
			this.day =28;
		}
		else 
        {
			this.year-=value;
		}
    }
	
	public void decrementMonth() //-3
	{
		if (this.month == 3 && this.year % 4 == 0) 
		{
			this.month -=1;
			this.day = 29;
			
		}
		else if ( this.month==3 && this.year % 4 != 0) {
			this.month -=1;
			this.day = 28;
		}

        else
        {
            this.month -=1;

        }
	}

    public void decrementMonth(int value) //-33 
	{
        if(this.month < value)
        {
            this.year-=1;
            this.month = this.month + 12 - value;
            if((this.month == 4 ||this.month == 6  ||this.month ==9  ||this.month == 11) && this.day >30)
            {
                this.day = 30;
            }
            else if ( this.month == 2 && this.day >29 && this.year % 4 == 0)
            {
                this.day =29;
            }
            else if ( this.month == 2 && this.day >28 && this.year % 4 != 0)
            {
                this.day =28;
            }
        }

        else 
        {
            this.month -= value;
            if((this.month == 4 ||this.month == 6  ||this.month ==9  ||this.month == 11) && this.day >30)
            {
                this.day = 30;
            }
            else if ( this.month == 2 && this.day >29 && this.year % 4 == 0)
            {
                this.day =29;
            }
            else if ( this.month == 2 && this.day >28 && this.year % 4 != 0)
            {
                this.day =28;
            }


        }
    }
	public void incrementYear() //22
	{
		this.year+=1;
		
	}
	
	public void incrementMonth() //3
	{
		this.month+=1;
		
	}
	
	public void incrementMonth(int value) //33
	{
		this.year += (value + this.month) / 12;
        this.month = (this.month + value) % 12; 
		if (this.month==2 && this.year % 4 == 0 && this.day >29) 
		{
			this.day =29;
		}
        else if((this.month == 4 ||this.month == 6  ||this.month ==9  ||this.month == 11) && this.day >30)
        {
            this.day = 30;
        }
		else if(this.month == 2 && this.year % 4 != 0 && this.day > 29) 
		{
			this.day = 28;
		}
		
		
	}
	
	public void decrementDay(int value) {
		for(;value>0;value--) 
		{
			boolean secMonth = (this.year % 4 == 0 );
			boolean month31 = (this.month == 1 || this.month==3 || this.month==5 || this.month == 7 || this.month ==8 ||this.month==10 );
			boolean lastMonth = (this.month == 12);
			boolean month30 = (this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11 );
			
			if(this.day== 1 && this.month == 1) 
			{
				this.day =31;
				this.month= 12;
				
			}
			
			else if (this.day == 1 && this.month==3 && this.year % 4 == 0) 
			{
				this.day = 29;
				this.month-=1;
			}
			
			else if (this.day==1 && this.month == 3 && this.year % 4 != 0 ) 
			{
				this.day = 28;
				this.month = 2;
			}
			
			else if(month30 && this.day == 1) {
				this.day = 31;
				this.month -=1;
			}
			
			else 
            {
				this.day-=1;
			}
		}
			
			
	}
	public boolean isAfter(MyDate anotherDate) {
		if(this.year > anotherDate.year) 
		{
			return true;
		}
		else if (this.year == anotherDate.year && this.month > anotherDate.month) 
		{
			return true;	
		}
		
		else if(this.year == anotherDate.year && this.month== anotherDate.month && this.day > anotherDate.day) 
		{
			return true;
		}
		
		else 
		{
			return false;
		}
	}
	
	public boolean isBefore(MyDate anotherDate) {
		if(this.year < anotherDate.year) 
		{
			return true;
		}
		else if (this.year == anotherDate.year && this.month < anotherDate.month) 
		{
			return true;	
		}
		
		else if(this.year == anotherDate.year && this.month== anotherDate.month && this.day < anotherDate.day) 
		{
			return true;
		}
		
		else 
		{
			return false;
		}
	}

    public int dayDifference(MyDate anotherDate){
        int a=0;
        int b=0;
        int c=0;
        int difMonth= anotherDate.month;
        if(isBefore(anotherDate)){
            a=(anotherDate.year-this.year)*365;
            if(anotherDate.month>this.day){
                b=anotherDate.day-this.day;
                while(difMonth != this.month){
                    if(difMonth==1||difMonth==3||difMonth==5||difMonth==7||difMonth==8||difMonth==10||difMonth==12){
                        c+=31;
                    }
                    else if(difMonth==4||difMonth==6||difMonth==9||difMonth==11){
                        c+=30;
                    }
                    else if(anotherDate.year%4==0){
                        c+=29;
                    }
                    else{
                        c+=28;
                    }
                    difMonth-=1;
                }

            }
            else{
                b=this.day-anotherDate.day;
                while(difMonth != this.month){
                    if(difMonth==1||difMonth==3||difMonth==5||difMonth==7||difMonth==8||difMonth==10||difMonth==12){
                        c+=31;
                    }
                    else if(difMonth==4||difMonth==6||difMonth==9||difMonth==11){
                        c+=30;
                    }
                    else if(anotherDate.year%4==0){
                        c+=29;
                    }
                    else{
                        c+=28;
                    }
                    difMonth+=1;
                }

            }
            
        }
        if(isAfter(anotherDate)){
            a=(this.year-anotherDate.year)*365;
            if(anotherDate.month>this.day){
                b=anotherDate.day-this.day;
                while(difMonth != this.month){
                    if(difMonth==1||difMonth==3||difMonth==5||difMonth==7||difMonth==8||difMonth==10||difMonth==12){
                        c+=31;
                    }
                    else if(difMonth==4||difMonth==6||difMonth==9||difMonth==11){
                        c+=30;
                    }
                    else if(anotherDate.year%4==0){
                        c+=29;
                    }
                    else{
                        c+=28;
                    }
                    difMonth-=1;
                }

            }
            else{
                b=this.day-anotherDate.day;
                while(difMonth != this.month){
                    if(difMonth==1||difMonth==3||difMonth==5||difMonth==7||difMonth==8||difMonth==10||difMonth==12){
                        c+=31;
                    }
                    else if(difMonth==4||difMonth==6||difMonth==9||difMonth==11){
                        c+=30;
                    }
                    else if(anotherDate.year%4==0){
                        c+=29;
                    }
                    else{
                        c+=28;
                    }
                    difMonth+=1;
                }

            }
            
        }
        return a+b+c;
    }
}
	
		
		

	
	
	

